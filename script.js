// console.log("Hello World");

const getCube = 10 ** 3;
console.log(`The cube of 10 is ${getCube}`);

const address = ["J.P. Rizal St.", "Bgry. Crossing Bayabas", "Davao City"];

const [street, baranggay, city] = address;
console.log(`I live at ${street}, ${baranggay}, ${city}.`);

const animal = {
	name: "Lolong",
	animalType: "Saltwater Crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"


};

const {name, animalType, weight, length} = animal;
console.log(`${name} was a ${animalType}. He weighted at
${weight} with a measurement of ${length}.`)


const numbers = [1, 2, 3, 4, 5];
numbers.forEach( (number) => {
	console.log(number);
});


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);